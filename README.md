# Childfocus notfound

The Childfocus Notfound module changes your 404 pages to support the Childfocus
Notfound project. Every 404 page will tell you about a child that is missing,
just like the page you where looking for.

For more information on the Notfound project, visit
[Notfound organisation](https://notfound.org/).

The D9 version of this module uses a block which you can place in the content
region of your theme. A new visibility condition "Childfocus (notfound.org)" is
available which you need to configure if you which to see the block only on a
404 page. Open the block configuration and goto "Childfocus (notfound.org)"
visibility tab and check the "Show in page not found" to only see this block in
your 404 page.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Get a key on [Notfound organisation](https://notfound.org) (you'll have to
  copy it from the embed-code).
- Copy that key in your configuration at `/admin/config/childfocus_notfound`.
