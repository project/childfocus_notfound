<?php

namespace Drupal\childfocus_notfound\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ChildfocusNotfoundForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'childfocus_notfound_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'childfocus_notfound.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('childfocus_notfound.settings');

    $form['#tree'] = true;
    $form['#title'] = $this->t('Childfocus notfound settings');

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => $this->t("Get a key on <a href=\"http://notfound.org\">http://notfound.org</a> (you'll have to copy it from the embed-code)."),
      '#default_value' => $config->get('key') ?? ''
    ];

    $form['fallback_langcode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Fallback langcode'),
      '#description' => $this->t('Notfound.org only supports Dutch, French and English. Set a fallback language in case your site is in a different language.'),
      '#options' => [
        'en' => \Drupal::languageManager()->getLanguageName('en'),
        'nl' => \Drupal::languageManager()->getLanguageName('nl'),
        'fr' => \Drupal::languageManager()->getLanguageName('fr'),
      ],
      '#default_value' => $config->get('fallback_langcode') ?? 'en'
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('childfocus_notfound.settings');

    $config->set('key', $form_state->getValue('key'));
    $config->set('fallback_langcode', $form_state->getValue('fallback_langcode'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
