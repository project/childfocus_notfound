<?php

namespace Drupal\childfocus_notfound\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a child focus block.
 *
 * @Block(
 *   id = "childfocus_notfound",
 *   admin_label = @Translation("Childfocus (notfound.org)"),
 * )
 */
class ChildfocusNotfound extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /** @var \Drupal\Core\Language\LanguageManagerInterface $languageManager */
  private $languageManager;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $languageManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = \Drupal::config('childfocus_notfound.settings');

    $supported_languages = ['en' => 'en-BE', 'fr' => 'fr-BE', 'nl' => 'nl-BE'];
    $current_langcode = $this->languageManager->getCurrentLanguage()->getId();
    $fallback_langcode = $config->get('fallback_langcode') ?? 'en';

    $langcode = $supported_languages[$current_langcode] ?? $supported_languages[$fallback_langcode];
    $key = Html::escape($config->get('key')) ?? '';

    $url = "https://notfound-static.fwebservices.be/$langcode/404?key=$key";

    $iframeTitle = $this->t('Child focus not found');

    $html = <<<HTML
        <iframe title="$iframeTitle" src="$url" width="100%" height="650" frameborder="0"></iframe>
    HTML;

    return [
      '#markup' => Markup::create($html),
      '#cache' => [
        'contexts' => [
          'languages:' . LanguageInterface::TYPE_INTERFACE,
        ],
      ]
    ];
  }

}
