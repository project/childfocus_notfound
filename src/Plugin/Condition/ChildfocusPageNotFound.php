<?php

namespace Drupal\childfocus_notfound\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'Page Not Found' condition.
 *
 * @Condition(
 *   id = "childfocus_notfound",
 *   label = @Translation("Childfocus (notfound.org)"),
 * )
 */
class ChildfocusPageNotFound extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('request_stack'),
      $configuration,
      $plugin_id,
      $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['show_on_page_not_found' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['prefix'] = ['#markup' => '<h5>Childfocus (notfound.org)</h5>'];
    $form['show_on_page_not_found'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show in page not found'),
      '#default_value' => $this->configuration['show_on_page_not_found'] ?? 0
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $flag = $form_state->getValue('show_on_page_not_found');
    if ($flag) {
      $this->configuration['show_on_page_not_found'] = $form_state->getValue('show_on_page_not_found');
    }
    else {
      unset($this->configuration['show_on_page_not_found']);
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Constructs a Page not found condition plugin.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(RequestStack $request_stack, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $request_stack;
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    // Get the configuration of the page not found.
    $page_not_found_checked = $this->configuration['show_on_page_not_found'];

    // Don't apply if show "in page not found" is not checked
    // or "show in page not found" was passed from checked to unchecked.
    if (empty($page_not_found_checked) && !$this->isNegated()) {
      return TRUE;
    }

    $request = $this->requestStack->getCurrentRequest();
    $status = $request->attributes->get('exception');

    if ($status && $status->getStatusCode() === 404) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    if (!empty($this->configuration['negate'])) {
      return $this->t('Do not return true on page not found.');
    }
    return $this->t('Return true on page not found.');

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'url.path';
    return $contexts;
  }

}
